# shopping-list

Simple repository to practise git basics on a shopping list. Simply add things on the list by editing the list.txt file on a repository you will have cloned. Then commit, push things on the remote repo and check the result on the gitlab interface.



Exercices
=========

Creating the project
--------------------

1. Fork the project on the web interface. 1 fork per group and all group members should be member of the forked git repo.

2. Each group member clones the project on his/her laptop 

Looking at diffs
----------------
1. Modify list.txt of your working copy and try 
  ```
  git status
  git diff 
  git diff --staged
  ```

2. Add this file to the staging area and try
  ```
  git status
  git diff 
  git diff --staged
  ```

3. Remove the file from the staging area (*reset*, Hint: the command is shown in the output of git status). 
4. Then cancel your modification by asking git to overwrite the modified file with the latest version of the repository. 
```
git checkout master -- list.txt
```

Working on the same branch
--------------------------

1. Fast forward merges
On the master branch, for each group of students:
  * student1 modifies list.txt, commits and pushes
  ```
  vim list.txt
  git commit 
  git push origin master
  ```
  * student2 pulls, modifies list.txt, commits and pushes
  ```
  git pull origin master
  vim list.txt
  git commit 
  git push origin master
  ```
  * student3 pulls, modifies list.txt, commits and pushes
  ```
  git pull origin master
  vim list.txt
  git commit 
  git push origin master
  ```

2. Merges
On the master branch, for each group of students:
  * everybody modifies list.txt, but a **different line**
  * everybody tries to commit & push
  
3. Conflict
On the master branch, for each group of students:
  * student1 and student2 modify **the same line** in list.txt 
  * everybody tries to commit & push
  
Feature branch & merge request
------------------------------
1. Protected branch
  * On the web interface, modify the access property of the master branch to *protected* status (settings/Repository/protected branches)
  * Student1 creates a new commit on the master branch
  * Try to push this new commit on the gtilab server on the master branch.
  * What happens ?

2. Feature branch
  * Student1 creates a new branch (`git branch branch_name`) such that the latest commit belongs to the new branch and not anymore to the master branch
  * Student1 pushes his new branch on the server (`git push origin branch_name`)
  * Student2 creates a new branch from master directly (`git checkout -b branch_name2`)
  * Student2 creates a new commit on this new branch and pushes the branch on the server

3. Merge request
  * On the web interface each student creates a merge request to merge their feature branch to the master one
  * Accept it and verify it gives the correct result

Setting and fixing issues
-------------------------
1. Each student creates a new feature branch like before and pushes it on the server

2. Reporting an issue
  * On the web interface, report an issue on the branch of your colleague
  * Discuss on the issue using the web interface

3. Fixing the issue
  * Each student fixes the issue in his own branch
  * Change the status of the issue by providing the appropriate commit message by mentioning `Fix #2` to close issue number 2 for example. Please refer to [gitlab documentation](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html) for all possible ways to open and close issues.

Exploring the history
---------------------
1. Try to answer the following questions 
  * Who touched this line from this file ?
  * Who modified this part of the reopsitory during a given period ?
  * To which branch does this commit belong ?
  * ...

2. If you managed to answer some questions, show the others how you managed to do it

Stashing modifications
----------------------
1. Stash modifications
  * Modify the local copy of a branch
  * You cannot checkout another branch right now. You have to *save* your modification without creating a proper commit, i.e. to *stash* them with `git stash`.
  * Checkout the other branch
  * Modify it and commit

2. Unstash your modifications
  * Checkout the inital branch again
  * unstash your modfications with `git stash pop`
  * You recover the state you were before
 
Multiple remote repositories
---------------------
1. Add the project of another group of students as a new remote of your local repository
2. Try to merge something and see what happens...

